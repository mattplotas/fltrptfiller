# this runner may be beyond saving. pending rewrite (maybe even the whole thing in C# at this point)


import sys, csv, os, subprocess, datetime, pdb
from posfile import *
from tkinter import Tk
from tkinter.filedialog import askdirectory
from enum import Enum
from Frame import *
from Header import *
from IndexFile import *
from ProjTypes import *
from Roll import *
from Run import *
from pathlib import Path
from shutil import copy2

Tk().withdraw()

strForm = '%7s%10s%7d%7d%7s%7s%7s%7d%20s'

pdop = svs = cloudsstr = visstr = turbstr = tempstr = fulltailstr = pilotstr = operstr = camstr = camtype = dustr = engon = engoff = abgpson = kinon = kinoff = abgpsoff = dep = arr = ''
lift = '1'
fstop = '5.6'
shutter = '350'
pirlist = []

def printstrBKP(single):
    print(strForm % (single.cardinal, single.runNum, single.fromExp, single.toExp, single.start[0:5], single.fstop, single.shutter, single.alt, single.notes))
    
def printstr(single):
    return (str(single.cardinal) + "," + str(single.runNum) + "," + str(single.fromExp) + "," + str(single.toExp) + "," + str(single.start[0:5]) + "," + str(single.alt) + ',' + str(single.notes))

def write_inline_to_pir(instring, rollObj):
    rollObj.pirstr += instring
    return
    
# horrific legacy code. 
def export_indexfile_csv(indfile):
    totlines = 0
    totexp = 0
    missionstr = ''
    for ro in indfile.rolls:
        f = open('output/csv/' + str(ro.num) + '.csv', 'w')
        print("")
        missionstr+=ro.area + '\n'
        f.write(ro.num + "," + ro.area + "," + str(ro.header.pdop) + " ," + str(ro.header.svs) + "," + ro.header.cloudsstr + "," + ro.header.visstr + "," + ro.header.turbstr + "," + ro.header.tempstr + "," + ro.header.pilotstr + "," + ro.header.operstr + "," + ro.header.dustr + "," + ro.header.engon + "," + ro.header.engoff + "," + ro.header.abgpson + "," + ro.header.kinon + "," + ro.header.kinoff + "," + ro.header.abgpsoff +  "," + ro.header.lift + "," + ro.header.camstr + " " + str(ro.header.camtype) + "," + ro.header.dep + "," + ro.header.arr + "," + ro.runs[0].fstop + "," + ro.runs[0].shutter + ',' + ro.header.fulltailstr + "\n")
        print(ro.num + ": " + ro.area + " " + "PDOP: " + str(ro.header.pdop) + " SVS: " + str(ro.header.svs))
        print(strForm.replace('d','s') % ('DIR','LINE', 'FROM', 'TO', 'START', 'FSTOP', 'SHUT','ALT', 'NOTES'))
        for ru in ro.runs:
            totlines+=1
            totexp+=len(ru.frames)
            ru.setTime()
            ru.avgAlt()
            ru.setDir(ro.projtype == ProjTypes.NRI_SV)
            ru.setFrames()
            printstrBKP(ru)
            f.write(printstr(ru) + "\n");
        f.close()
    
    print('\nAcquisition email:\n\n' + missionstr)
    print(indfile.rolls[0].header.fulltailstr + '/' + indfile.rolls[0].header.camstr + ' ' + indfile.rolls[0].header.camtype)
    print(indfile.rolls[0].header.pilotstr + '/' + indfile.rolls[0].header.operstr)
    print(totlines)
    print(totexp)
    return

def output_csv_file():
    return

def output_csv_term():
    return

def run_ExcelInterfacer(interfacerdir, parsedcsvsdir, templatedir, outputdir):
    #print('excel disabled')
    subprocess.run([interfacerdir, parsedcsvsdir, templatedir, outputdir])

def lift_prompt():
    return input("diff lift than " + lift + "?: ").lower() == 'y'
    
def weather_prompt():
    return input("diff weather vals? ").lower() == 'y'
    
def camsettings_prompt():
    return input("diff cam settings than " + fstop + "@" + shutter + "? ").lower() == 'y'

def get_weather_input():
    global cloudsstr, visstr, turbstr, tempstr
    cloudsstr = input("clouds: ")
    visstr = input("vis: ")
    turbstr = input("turb: ")
    tempstr = input("temp: ")

def get_init_input():
    global pilotstr,operstr,camstr,dustr, camtype, fulltailstr
    pilotstr = input("pilot: ")
    operstr = input("oper: ")
    fulltailstr = input("full tail: ")
    camstr = input("cam serial: ")
    camtype = input("camtype: ")
    dustr = input("du: ")

def get_lift_input():
    global lift, dep, arr, engon, engon, engoff, abgpson, kinon, kinoff, abgpsoff
    lift = input("lift: ")
    dep = input("dep ap: ")
    arr = input("arr ap: ")
        
    if (input("get times from posfiles for lift "+lift+"?: ").lower() == 'y'):
        posfolder_str = askdirectory()
        posfolder = posfile_folder(posfolder_str + '/')
        st = posfolder.start()
        en = posfolder.end()
        
        print(str(st) + ' to ' + str(en))
        
        engon = (st - datetime.timedelta(hours=6,minutes=5)).strftime('%H%M')
        kinon = (st + datetime.timedelta(minutes=3)).strftime('%H%M')
        abgpson = st.strftime('%H%M')
        
        kinoff = abgpsoff = en.strftime('%H%M')
        engoff = (en - datetime.timedelta(hours=6)).strftime('%H%M')
    else:
        engon = input("engine on: ")
        engoff = input("engine off: ")
        abgpson = input("abgps on: ")
        kinon = input("kin on: ")
        kinoff = input("kin off: ")
        abgpsoff = input("abgps off: ")

def get_camsettings_input():
    global fstop, shutter
    fstop = input("fstop: ")
    shutter = input("shutter: ")
    
def update_headers(head):
    # TODO this could be shorter
    head.pdop = pdop
    head.svs = svs
    head.cloudsstr = cloudsstr
    head.visstr = visstr
    head.turbstr = turbstr
    head.tempstr = tempstr
    head.pilotstr = pilotstr
    head.operstr = operstr
    head.camstr = camstr
    head.camtype = camtype
    head.dustr = dustr
    head.engon = engon
    head.engoff = engoff
    head.abgpson = abgpson
    head.kinon = kinon
    head.kinoff = kinoff
    head.abgpsoff = abgpsoff
    head.lift = lift
    head.dep = dep
    head.arr = arr
    head.fulltailstr = fulltailstr
get_init_input()
get_lift_input()

for filename in os.listdir('input'):
    if (filename.endswith('.csv') or filename.endswith('.txt') or filename.endswith('.CSV') or filename.endswith('.TXT')):
        #inreader = csv.reader(open('input/' + filename))
        inreader = open('input/' + filename)
        if (os.stat('input/' + filename).st_size > 0):
            #read headers, once per file
            
            #dependent on header size
            for i in range(6): next(inreader)
            
            # TODO could this be better? not depeendent on header order, but is dependent on all being present
            #need trackairconfig class
            headers = next(inreader).split(',')
            idUTC = headers.index('UTC TIME')
            idRUN = headers.index('RUN')
            idCOG = headers.index('COG')
            idLAT = headers.index('WGS LATITUDE')
            idLONG = headers.index('WGS LONGITUDE')
            idHEIGHT = headers.index('GPS HEIGHT')
            idFRAME = headers.index('FRAME')
            idFILM = headers.index('FILM')
            idAREA = headers.index('AREA NAME')
            idPHOTOID = headers.index('PHOTO IDENTIFIERS')
            idACCEPTED = headers.index('ACCEPTED')
            idPDOP = headers.index('PDOP')
            idSVS = headers.index('SVS') 
            idPROJTYPE = headers.index('PROJ NAME')
            idDATE = headers.index('DATE')
            
            
            # super dirty and ugly.
            # why can't i just intialize variables in python :(
            indexFl = IndexFile('m')
            first = 1
            currRollObj = Roll(0,'0','0','0','0000000',0)
            currRunObj = Run(-1)
                
            for row in inreader:
                tempfield = []
                for field in row.split(','):
                    tempfield.append(field)
                    
                newrollbool = False
                newNum = tempfield[idRUN]
                newRoll = tempfield[idFILM]
                
                #way too nested
                
                if (not tempfield[idRUN] == 'Free shot'):
                    if (not indexFl.rollNumExists(newRoll)):
                    
                        currRollObj = indexFl.addRoll(Roll(newRoll,tempfield[idAREA], tempfield[idPROJTYPE], camtype, fulltailstr, lift))

                        if (lift_prompt()):
                            get_lift_input()
                        if (weather_prompt()):
                            get_weather_input()
                        if (camsettings_prompt()):
                            get_camsettings_input()
                            
                        pdop = tempfield[idPDOP]
                        svs = tempfield[idSVS]
                        update_headers(currRollObj.header);
                            
                        isNewRoll = True
                    else:
                        currRollObj = indexFl.getRollByNum(newRoll)
                        
                    # needs to be broken up. this handles erroneous duplicate photoID's produced by track'air (apparently known behavior)
                    # this is needed to handle multiple attempts at the same line/frame by checking if a flown photoID already exists within a Run
                    # 
                    # TODO correct these indexes first seperately, then run them through. still would have some edge cases, ie if it is the first or last frame of a line so there may not be any avoiding this issue
                    if (isNewRoll or tempfield[idRUN] != currRunObj.runNum or (tempfield[idPHOTOID] != '' and currRunObj.frameNumExists(tempfield[idPHOTOID]) and (datetime.datetime.strptime(tempfield[idUTC][0:8], "%H:%M:%S") - datetime.datetime.strptime(currRunObj.frames[len(currRunObj.frames)-1].utc[0:8], "%H:%M:%S") > datetime.timedelta(seconds=15)))):
                        currRunObj = currRollObj.addRun(Run(newNum))
                        
                        # adds county/zone to flight notes for each site, as the line number for SV projects is arbitrary
                        if (currRollObj.projtype == ProjTypes.NRI_SV):    
                            currRunObj.notes = tempfield[idPHOTOID][0:13]
                            
                        currRunObj.fstop = fstop
                        currRunObj.shutter = shutter
                        
                        isNewRoll = False
                    
                    # null protects photo identifier
                    fixedPhotoId = -1 if not tempfield[idPHOTOID] else tempfield[idPHOTOID]
                       
                    currRunObj.addFrame(Frame(tempfield[idUTC], float(tempfield[idLAT].replace(" ",'')), float(tempfield[idLONG].replace(" ",'')),float(tempfield[idHEIGHT]), float(tempfield[idFRAME]), fixedPhotoId, tempfield[idCOG])) #*3.28 for m
                    write_inline_to_pir(row, currRollObj)
                    
            export_indexfile_csv(indexFl)

subprocess.run(["res\\excelinterfacer\\ExcelInterfacer.exe","C:\\Users\\mPlotas\\Documents\\fltrptfiller\\output\\csv", "C:\\Users\\mPlotas\\Documents\\fltrptfiller\\res\\KAS_DigitalFlightReport_2019.xlsx", "C:\\Users\\mPlotas\\Documents\\fltrptfiller\\output\\xlsx\\"])       
