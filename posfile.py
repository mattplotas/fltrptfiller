import datetime
import os
import re

#possibly regex-ify input
#pattern to line nums?

class posfile:
    def __init__(self, filepath):
        self.procd = 0
        self.filepath = filepath
        self.first = -99
        self.last = -99
        
    def __lt__(self, other):
        return self.first < other.first
        
    def proc(self):
        fst = 1
        in_dt = ''
        pattern = re.compile(r'(?P<time>\d{6}).\d{2},(?P<north>\d{4}\.\d{8}),N,(?P<west>\d{5}\.\d{8}),W,\d+,\d+,\d+\.\d+,(?P<altitude>\d+\.\d+),M,')
        inreader = open(self.filepath,errors='ignore')
        for row in inreader:
            if 'ZDA' in row:
                try:
                    in_dt = datetime.datetime.strptime(row[7:13] + row[17:27], '%H%M%S%d,%m,%Y')
                    if (fst):
                        self.first = in_dt
                        fst = 0
                except:
                    pass
            elif ('GGA' in row):
                try:
                    matches = pattern.search(row)
                    
                    north = str(float(matches.group('north'))/100)
                    ndeg = float(north[0:2])
                    nmin = float(north[3:5])
                    nsec = float(north[5:7])/100.0
                    north = str(ndeg + (nmin/60.0) + (nsec/60.0))
                    
                    west = str(float(matches.group('west'))/100)
                    wdeg = float(west[0:2])
                    wmin = float(west[3:5])
                    wsec = float(west[5:7])/100.0
                    west = str(wdeg + (wmin/60.0) + (wsec/60.0))
                    
                    coord_str += ' -' + west + ',' + north + ',' + matches.group('altitude')
                except:
                    pass
        self.last = in_dt
        procd = 1
       
    def start(self):
        if (not self.procd):
            self.proc()
        return self.first
        
    def end(self):
        if (not self.procd):
            self.proc()
        return self.last
        
class posfile_folder:
    def __init__(self, dirpath):
        self.procd = 0
        self.dirpath = dirpath
        self.files = []
        self.first = -99
        self.last = -99
    
    def proc(self):
        filelist = os.listdir(self.dirpath)
        for filename in filelist:
            temp_posfile = posfile(self.dirpath + filename)
            temp_posfile.proc()
            self.files.append(temp_posfile)
        self.files = sorted(self.files);
        self.first = self.files[0].start()
        self.last = self.files[len(self.files)-1].end()
        procd = 1
            
    def start(self):
        if (not self.procd):
            self.proc()
        return self.first
        
    def end(self):
        if (not self.procd):
            self.proc()
        return self.last    