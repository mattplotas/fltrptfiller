from ProjTypes import *
from Header import *
import re

liftchar = ['','','b','c','d','e','f']

#HORRIBLY dependent... projtypes... argument forms... all that

class Roll:
    def __init__(self, num, area, projtype, camtype, fulltailstr, lift):
        self.header = Header()
        self.num = num
        self.runs = []
        self.pirstr = ''
        
        pattern = re.compile('.*')
        prefix = ''
        postfix = ''
        
        if projtype.startswith("SLI"):
            self.projtype = ProjTypes.SLI
            area = area.replace('_ext','')
            pattern = re.compile(r'(?P<area>[a-zA-Z]{2}_(z|Z)\d{2})')
            camstr = camtype
            fulltailstr = fulltailstr[-3:] + lift
            date = num[0:4] + '20' + num[4:6]
            prefix = 'SLI_'
            
        elif projtype.startswith("NRI") and projtype.endswith('_SV'):
            self.projtype = ProjTypes.NRI_SV
            pattern = re.compile(r'(?P<area>NRI20\d{2})')
            camstr = camtype
            fulltailstr = fulltailstr[-3:] + lift
            date = '20' + num[4:6] + num[0:4]
            
        elif projtype.startswith("NRI"):
            self.projtype = ProjTypes.NRI
            area = (area[12]+ area[13] + '_z' + area[8] + area[9])
            pattern = re.compile(r'(?P<area>[a-zA-Z]{2}_(z|Z)\d{2})')
            camstr = camtype
            fulltailstr = fulltailstr[-3:] + lift
            date = num[0:4] + '20' + num[4:6]
            prefix = 'NRI_'
            
        elif projtype.startswith("US"):
            self.projtype = ProjTypes.UCO
            pattern = re.compile(r'(?P<area>[A-Z]{7,8})')
            camstr = camtype
            fulltailstr = fulltailstr[-3:]
            postfix = liftchar[int(lift)]
            date = num[4:6] + num[0:4]
            
        elif projtype.startswith("VDP"):
            self.projtype = ProjTypes.VDP
            pattern = re.compile(r'[A-Z]{2}_(?P<area>([A-Z]\d\d){2})_(?P<camera>UC.{1,3})_.*')
            camstr = camtype[2:3]
            fulltailstr = fulltailstr
            date = num[4:6] + num[0:4]
            postfix = liftchar[int(lift)]
            
        elif projtype.startswith("KAS"):
            self.projtype = ProjTypes.TOPO
            pattern = re.compile(r'(?P<area>\d{2}[A-Z]{2}-?\d{3,5})_')
            camstr = camtype
            date = num[0:4] + '20' + num[4:6]
            fulltailstr = fulltailstr[-3:] + lift
            prefix = 'KAS_'
            
        matches = pattern.search(area)
        
        if not projtype.startswith("0"):
            #print('area: ' + prefix + matches.group('area').replace('-','').replace('_','').upper()+ '_' + camstr.replace('m3','') + '_' +fulltailstr + '_' + date + postfix)
            self.area = prefix + matches.group('area').replace('-','').replace('_','').upper() + '_' + camstr.replace('m3','') + '_' +fulltailstr + '_' + date + postfix
        
        self.sheets = []

    def addRun(self, run):
        self.runs.append(run)
        return self.runs[len(self.runs)-1]