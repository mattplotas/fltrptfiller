class IndexFile:
    def __init__(self, units):
        self.rolls = []
        self.units = units
        
    def addRoll(self, roll):
        self.rolls.append(roll)
        return roll
        #return self.rolls[len(self.rolls)-1]
          
    def rollNumExists(self,num):
        for r in self.rolls:
            if ( num == r.num ):
                return 1
        return 0
        
    def getRollByNum(self,num):
        for r in self.rolls:
            if ( num == r.num ):
                return r
        return 0