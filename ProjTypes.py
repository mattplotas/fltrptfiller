from enum import Enum
class ProjTypes(Enum):
    TOPO = 0 #DIGITAL,         21PA-288_UCFp,               KAS_21PA288_FCAM_1TA#_MMDDYYYY
    SLI = 1 #SLI21,            SLI21_LA_z15_16_UCFp,        SLI21_CTZ18_FCAM_1TA#_MMDDYYYY
    NRI = 2 #NRI21,            NRI21_NJ_z18_UCFp,           NRI21_CTZ18_FCAM_1TA#_MMDDYYYY
    VDP = 3 #VDP21,            TN_N35W90_UCEm3_14cm_v2,     N35W88_C_FTAIL_YYMMDD(b)
    UCO = 4 #USMDABE_O3,       USMDABE-B_Aberdeen_O,        OR_Medford_C_FTAIL_YYMMDD(b)
    NRI_SV = 5                                                      # %p_%a_%c_%t_%d