#does this need to be named run-attempt?

class Run:
    def __init__(self, runNum):
        #this cannot be good nor safe. but at least it's fairly obvious if it fails.
        self.alt = -999
        self.cardinal = ''
        self.start = 0
        self.fromExp = -99
        self.toExp = -99
        self.fstop = -99
        self.shutter = -99
        self.notes = ''
        self.runNum = runNum
        self.frames = []

    def addFrame(self, frame):
        self.frames.append(frame)
        
    def avgAlt(self):
        sm = 0
        for f in self.frames:
            sm += f.height
        self.alt = sm/len(self.frames)
        return self.alt
        
    def setDir(self, sv):
        #switching on type... bad!
        if (sv):
            dirs = ['N','E','S','W']
            self.cardinal = dirs[int(((float(self.frames[0].cog) + 45.0)%360)/90)]
        else:
            latdif = self.frames[len(self.frames) - 1].lat - self.frames[0].lat
            longdif =  self.frames[len(self.frames) - 1].long - self.frames[0].long

            if (abs(latdif) > abs(longdif)):
                self.cardinal = 'N' if latdif > 0 else 'S'
            else:
                self.cardinal = 'E' if longdif > 0 else 'W'
                
    def setTime(self):
        self.start = self.frames[0].utc
     
    def setFrames(self):
        self.fromExp = self.frames[0].ind
        self.toExp = self.frames[len(self.frames)-1].ind
        
    def frameNumExists(self,num):
        numFixed = num if num or num == "" else -1
        for r in self.frames:
            if (numFixed == r.identifier):
                return 1
        return 0