Outputs human readable + sanitized tables consistent with flight reporting sheets.

Takes one or more snapBASE Photo Index csv's (within ./input/, must be .csv or .txt)

THIS IS NOT A REPLACEMENT FOR HANDRWRITTEN LOGS. 
This is a supplemental tool for easily recreating unreadable data, or if necessary, completely recreating a flight report.

NYI:
~~export into full graphical sheet~~ with ExcelInterfacer
error handling of any significance (it skips empty files but that's about it)
allow arbitrary file location via arguments
possible gui

whole bunch of other stuff probably. This is more of an exercise than an actual tool so use with caution.
