#this can be an empty class; just for organization

class Frame:
    def __init__(self, utc, lat, long, height, ind, identifier, cog):
        self.utc = utc
        self.height = height
        self.ind = ind
        self.identifier = identifier
        self.lat = lat
        self.long = long
        self.cog = cog