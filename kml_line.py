import sys, os, datetime, re
    
#https://ourairports.com/data/

#out_str = '<?xml version="1.0" encoding="UTF-8"?>\n<kml xmlns="http://www.opengis.net/kml/2.2">\n<Document>\n<Placemark>\n<LineString>\n<altitudeMode>absolute</altitudeMode>\n<coordinates>$BODY\n</coordinates>\n</LineString>\n</Placemark>\n</Document>\n</kml>'

out_str = """<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
<Document>
  <Style id="linestyle">
    <LineStyle>
      <color>$KML_COLOR</color>
      <width>$KML_WEIGHT</width>
    </LineStyle>
  </Style>
  <Placemark>
    <styleUrl>#linestyle</styleUrl>
    <LineString>
      <altitudeMode>absolute</altitudeMode>
      <coordinates>
        $BODY
      </coordinates>
    </LineString>
  </Placemark>
</Document>
</kml>"""

coord_str = ''
kml_weight = ''
kml_color = ''
input_dir = ''

pattern = re.compile(r'(?P<time>\d{6}).\d{2},(?P<north>\d{4}\.\d{8}),N,(?P<west>\d{5}\.\d{8}),W,\d+,\d+,\d+\.\d+,(?P<altitude>\d+\.\d+),M,')

try:
    input_dir = sys.argv[1]
except:
    input_dir = '.'
try:
    kml_color = sys.argv[2] 
except:
    kml_color = 'ff0000ff'
try:
    kml_weight = sys.argv[3]
except:
    kml_weight = '4'

for filename in os.listdir(input_dir):
    inreader = open(input_dir+'/' + filename,errors='ignore')
    for row in inreader:
        if ('GGA' in row):
            try:
                matches = pattern.search(row)
                
                north = str(float(matches.group('north'))/100)
                ndeg = float(north[0:2])
                nmin = float(north[3:5])
                nsec = float(north[5:7])/100.0
                north = str(ndeg + (nmin/60.0) + (nsec/60.0))
                
                west = str(float(matches.group('west'))/100)
                wdeg = float(west[0:2])
                wmin = float(west[3:5])
                wsec = float(west[5:7])/100.0
                west = str(wdeg + (wmin/60.0) + (wsec/60.0))
                
                coord_str += ' -' + west + ',' + north + ',' + matches.group('altitude')
            except:
                pass

out_str = out_str.replace("$BODY",coord_str).replace("$KML_WEIGHT", kml_weight).replace("$KML_COLOR", kml_color)

f = open('doc.kml','w')
f.write(out_str)
f.close()